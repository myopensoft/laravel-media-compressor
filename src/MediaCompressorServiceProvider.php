<?php

namespace MyOpensoft\MediaCompressor;

use Spatie\LaravelPackageTools\Package;
use Spatie\LaravelPackageTools\PackageServiceProvider;
use MyOpensoft\MediaCompressor\Commands\MediaCompressorCommand;

class MediaCompressorServiceProvider extends PackageServiceProvider
{
    public function configurePackage(Package $package): void
    {
        /*
         * This class is a Package Service Provider
         *
         * More info: https://github.com/spatie/laravel-package-tools
         */
        $package
            ->name('media-compressor')
            ->hasConfigFile()
            ->hasViews()
            ->hasMigration('create_media-compressor_table')
            ->hasCommand(MediaCompressorCommand::class);
    }
}
