<?php

namespace MyOpensoft\MediaCompressor\Facades;

use Illuminate\Support\Facades\Facade;

/**
 * @see \MyOpensoft\MediaCompressor\MediaCompressor
 */
class MediaCompressor extends Facade
{
    protected static function getFacadeAccessor(): string
    {
        return \MyOpensoft\MediaCompressor\MediaCompressor::class;
    }
}
