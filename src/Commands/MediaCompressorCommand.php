<?php

namespace MyOpensoft\MediaCompressor\Commands;

use Illuminate\Console\Command;

class MediaCompressorCommand extends Command
{
    public $signature = 'media-compressor';

    public $description = 'My command';

    public function handle(): int
    {
        $this->comment('All done');

        return self::SUCCESS;
    }
}
